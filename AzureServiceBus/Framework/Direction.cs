﻿namespace AzureServiceBus.Framework
{
    public enum Direction
    {
        Inbound,
        Outbound
    }
}
