﻿using System;

namespace AzureServiceBus.Framework
{
    public interface IMessageQueue : IDisposable
    {
        string Address { get; }

        void CreateOutbound(string address, MessagePattern pattern);

        void CreateInbound(string address, MessagePattern pattern);
        
        void Send(Message message, string sessionKey = "");
        
        void Listen(Action<Message> onMessageReceived);

        void Receive(Action<Message> onMessageReceived);

        string GetAddress(string name);

        void AbandonMessage();

        void CompleteMessage();

        void DeferMessage();

    }
}
