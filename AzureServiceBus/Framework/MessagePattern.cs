﻿namespace AzureServiceBus.Framework
{
    public enum MessagePattern
    {
        SendAndForget,
        PublishSubscribe
    }
}
