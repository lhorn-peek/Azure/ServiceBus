﻿using System;

namespace AzureServiceBus.Framework
{
    public abstract class MessageQueueBase : IMessageQueue
    {
        public string Address { get; protected set; }

        public abstract void CreateOutbound(string name, MessagePattern pattern);

        public abstract void CreateInbound(string name, MessagePattern pattern);

        public abstract void Send(Message message, string sessionKey = "");

        public abstract void Listen(Action<Message> onMessageReceived);

        public abstract void Receive(Action<Message> onMessageReceived);

        public abstract string GetAddress(string name);

        public abstract void AbandonMessage();

        public abstract void CompleteMessage();

        public abstract void DeferMessage();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected abstract void Dispose(bool disposing);



        public MessagePattern Pattern { get; protected set; }

        protected Direction Direction { get; set; }

        protected void Initialise(Direction direction, string name, MessagePattern pattern)
        {
            Direction = direction;
            Pattern = pattern;
            Address = GetAddress(name);
        }

    }
}
