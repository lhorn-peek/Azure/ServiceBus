﻿using System.Collections.Generic;
using AzureServiceBus.Framework;
using AzureServiceBus.Providers.Azure;

namespace AzureServiceBus
{
    public static class MessageQueueFactory
    {
        private static Dictionary<string, IMessageQueue> _Queues = new Dictionary<string, IMessageQueue>();

        public static IMessageQueue CreateInbound(string name, MessagePattern pattern)
        {
            var key = $"{Direction.Inbound}:{name}:{pattern}";

            if (_Queues.ContainsKey(key))
                return _Queues[key];

            var queue = Create();

            queue.CreateInbound(name, pattern);

            _Queues[key] = queue;

            return _Queues[key];
        }

        public static IMessageQueue CreateOutbound(string name, MessagePattern pattern)
        {
            var key = $"{Direction.Outbound}:{name}:{pattern}";

            if (_Queues.ContainsKey(key))
                return _Queues[key];

            var queue = Create();

            queue.CreateOutbound(name, pattern);

            _Queues[key] = queue;

            return _Queues[key];
        }

        private static IMessageQueue Create()
        {
            return new ServiceBusMessageQueue();
        }
    }
}