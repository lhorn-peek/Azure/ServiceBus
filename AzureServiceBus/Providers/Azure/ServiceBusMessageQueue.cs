﻿using System;
using System.Configuration;
using System.IO;
using AzureServiceBus.Extensions;
using AzureServiceBus.Framework;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;

namespace AzureServiceBus.Providers.Azure
{
    public class ServiceBusMessageQueue : MessageQueueBase
    {
        #region init

        private QueueClient _queueClient;

        private Action<Message> _processMessage;

        private BrokeredMessage _currentRecievedMessage;

        private readonly string _connectionString;

        public ServiceBusMessageQueue()
        {
            _connectionString = ConfigurationManager.AppSettings["MessageQueue.AzureServiceBus.Connection"];
        }

        public ServiceBusMessageQueue(string connectionString)
        {
            _connectionString = connectionString;
        }

        #endregion

        public override void CreateOutbound(string name, MessagePattern pattern)
        {
            Initialise(Direction.Outbound, name, pattern);

            var factory = MessagingFactory.CreateFromConnectionString(_connectionString);

            factory.RetryPolicy = RetryPolicy.Default;

            _queueClient = factory.CreateQueueClient(Address);
        }

        public override void CreateInbound(string name, MessagePattern pattern)
        {
            Initialise(Direction.Inbound, name, pattern);

            var factory = MessagingFactory.CreateFromConnectionString(_connectionString);

            factory.RetryPolicy = RetryPolicy.Default;

            _queueClient = factory.CreateQueueClient(Address);
        }

        public override void Send(Message message, string sessionKey = "")
        {
            var brokeredMessage = new BrokeredMessage(message.ToJsonStream(), true);

            if (!string.IsNullOrEmpty(sessionKey))
                brokeredMessage.SessionId = sessionKey;

            _queueClient.Send(brokeredMessage);
        }

        public override void Listen(Action<Message> onMessageReceived)
        {
            _processMessage = onMessageReceived;

            var options = new OnMessageOptions
            {
                AutoComplete = false,
                AutoRenewTimeout = TimeSpan.FromMinutes(1)
            };

            _queueClient.OnMessage(Handle, options);
        }

        public override void Receive(Action<Message> onMessageReceived)
        {
            _processMessage = onMessageReceived;

            var brokeredMessage = _queueClient.Receive();

            Handle(brokeredMessage);

        }

        public override string GetAddress(string name)
        {
            //TODO -> get queue names from config file

            //list of created queues within an azure service bus namespace(namespace in conn string)
            switch (name.ToLower())
            {
                case "randomq":
                    return "randomq";

                case "anotherqueue":
                    return "anotherqueue";

                default:
                    return name;
            }
        }

        private void Handle(BrokeredMessage brokeredMessage)
        {
            _currentRecievedMessage = null;
            _currentRecievedMessage = brokeredMessage;

            var messageStream = brokeredMessage.GetBody<Stream>();
            var message = Message.FromJson(messageStream);

            _processMessage(message);
        }

        public override void AbandonMessage()
        {
            _currentRecievedMessage?.Abandon();
        }

        public override void CompleteMessage()
        {
            _currentRecievedMessage?.Complete();
        }

        public override void DeferMessage()
        {
            _currentRecievedMessage?.Defer();
        }

        protected override void Dispose(bool disposing)
        {
            //
        }
    }
}
